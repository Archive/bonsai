#!/usr/local/perl/bin/perl -w
# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# The contents of this file are subject to the Netscape Public
# License Version 1.1 (the "License"); you may not use this file
# except in compliance with the License. You may obtain a copy of
# the License at http://www.mozilla.org/NPL/
#
# Software distributed under the License is distributed on an "AS
# IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
# implied. See the License for the specific language governing
# rights and limitations under the License.
#
# The Original Code is the Bonsai CVS tool.
#
# The Initial Developer of the Original Code is Netscape Communications
# Corporation. Portions created by Netscape are
# Copyright (C) 1998 Netscape Communications Corporation. All
# Rights Reserved.
#
# Contributor(s): 


#
# Query the CVS database.
#

use diagnostics;
use strict;

# Shut up misguided -w warnings about "used only once".  "use vars" just
# doesn't work for me.

sub sillyness {
    my $zz;
    $zz = $::Setup_String;
    $zz = $::use_layers;
}

require 'CGI.pl';

$|=1;

my $CVS_ROOT = $::FORM{"cvsroot"};
$CVS_ROOT = pickDefaultRepository() unless $CVS_ROOT;

LoadTreeConfig();
$::TreeID = $::FORM{'module'} 
     if (!exists($::FORM{'treeid'}) &&
         exists($::FORM{'module'}) &&
         exists($::TreeInfo{$::FORM{'module'}}{'repository'}));
$::TreeID = 'default'
     if (!exists($::TreeInfo{$::TreeID}{'repository'}) ||
         exists($::TreeInfo{$::TreeID}{'nobonsai'}));


# get dir, remove leading and trailing slashes

my $dir = $::FORM{"dir"};
$dir = "" unless defined $dir;
$dir =~ s/^\/([^:]*)/$1/;
$dir =~ s/([^:]*)\/$/$1/;

my $rev = $::FORM{"rev"};

if(!defined($rev)) {
    $rev='';
}

print "Content-type: text/html\n\n";


my $registryurl = Param('registryurl');
$registryurl =~ s@/$@@;

my $script_str;

&setup_script;
$::Setup_String = $script_str;

if( $CVS_ROOT eq ""  ){
    $CVS_ROOT = pickDefaultRepository();
}

validateRepository($CVS_ROOT);

my $s = "";

if ($rev) {
    $s = "for branch <i>$rev</i>";
}

CheckHidden("$CVS_ROOT/$dir");

my $revstr = '';
$revstr = "&amp;rev=$rev" unless $rev eq '';
my $rootstr = '';
$rootstr .= "&amp;cvsroot=$::FORM{'cvsroot'}" if defined $::FORM{'cvsroot'};
$rootstr .= "&amp;module=$::TreeID";
my $module = $::TreeInfo{$::TreeID}{'module'};

my $toplevel = Param('toplevel');

# put HTML header, including an onClick handler to hide the popup
PutsHeader("Repository Directory $toplevel/$dir $s", undef, undef, 
           "onClick=\"hidePopup()\"");

my $output = "<DIV ALIGN=LEFT>";
$output .= "<A HREF='toplevel.cgi" . BatchIdPart('?') . "'>$toplevel</a>/ ";

my ($dir_head, $dir_tail) = $dir =~ m@(.*/)?(.+)@;
$dir_head = "" unless defined $dir_head;
$dir_tail = "" unless defined $dir_tail;
my $link_path = "";
foreach my $path (split('/',$dir_head)) {
    $link_path .= url_quote($path);
    $output .= "<A HREF='rview.cgi?dir=$link_path$rootstr$revstr'>$path</A>/ ";
    $link_path .= '/';
}
chop ($output);
$output .= " $dir_tail/ $s ";
$output .= "</DIV>";

print qq%
<iframe id="popupiframe" class=popup 
style="display:none" name="popupiframe">Frames not supported</iframe>\n
% if $::use_dom;
print $output;
print '<table width="100%"><tr><td width="70%">';

my $other_dir;

($other_dir = $dir) =~ s!^$module/?!!;
my $other_dir_used = 1;

LoadDirList();
if (-d "$CVS_ROOT/$dir") {
     chdir "$CVS_ROOT/$dir";
     $other_dir_used = 0;
} elsif (-d "$CVS_ROOT/$other_dir") {
     chdir "$CVS_ROOT/$other_dir";
} else {
     chdir "$CVS_ROOT";
}

print "
<FORM action=rview.cgi method=get>
<TABLE CELLPADDING=0 CELLSPACING=0>
<TR><TD>Goto Directory:</TD>
<TD><INPUT name=dir value='$dir' size=30>
<INPUT name=rev value='$rev' type=hidden>
<INPUT name=module value='$::TreeID' type=hidden>
<INPUT name=cvsroot value='$CVS_ROOT' type=hidden>
<INPUT type=submit value='chdir'>
</TD></TR>
</TABLE>
</FORM>
<FORM action=rview.cgi method=get>
<TABLE CELLPADDING=0 CELLSPACING=0>
<TR><TD>Branch:</TD>
<TD><INPUT name=rev value='$rev' size=30>
<INPUT name=dir value='$dir' type=hidden>
<INPUT name=module value='$::TreeID' type=hidden>
<INPUT name=cvsroot value='$CVS_ROOT' type=hidden>
<INPUT type=submit value='Set Branch'>
</TD></TR>
</TABLE>
</FORM>

";

my @dirs = ();


DIR:
while( <*> ){
    if( -d $_ ){
        push @dirs, $_;
    }
}



my $j;
my $split;

if( @dirs != 0 ){
    $j = 1;
    $split = int(@dirs/4)+1;
    print "<P><FONT SIZE=\"+1\"><B>Directories:</B></FONT><table><TR VALIGN=TOP><td><dl>";


    for my $i (@dirs){
        $::FORM{"dir"} = ($dir ne "" ? "$dir/$i" : $i);
        my $anchor = &make_cgi_args;
        print qq%<dt><a href="rview.cgi${anchor}">$i</a>\n%;
        if( $j % $split == 0 ){
            print "\n</dl></td><td><dl>\n";
        }
        $j++;
    }
    $::FORM{"dir"} = $dir;
    print "\n</dl></td></tr></table>\n";
}


print "<P><FONT SIZE=\"+1\"><B>Files:</B></FONT>";
print "<table><TR VALIGN=TOP><td><dl>";
my @files = <*,v>;
$j = 1;
$split = int(@files/4)+1;

for $_ (@files){
    $_ =~ s/\,v//;
    my $file = url_quote($_);
    my $dir = url_quote($dir);
    print qq{<dt><a href="$registryurl/file.cgi?cvsroot=$CVS_ROOT&amp;file=$file&amp;dir=$dir$revstr"}
          . " onclick=\"return js_file_menu('$dir','$file','$rev','$CVS_ROOT',event)\">\n";
    # print unquoted filename 
    print "$_</a>\n";
    if( $j % $split == 0 ){
        print "\n</dl></td><td><dl>\n";
    }
    $j++;
}
print "\n</dl></td></tr></table>\n</td><td valign=\"top\">";
cvsmenu("");
print "\n</td></tr></table>\n";

PutsTrailer();


sub setup_script {

if ($::use_layers) {
$script_str = qq%
<script $::script_type><!--
var event = new Object;

function js_who_menu(n,extra,d) {
    if( parseInt(navigator.appVersion) < 4 ||
        navigator.userAgent.toLowerCase().indexOf("msie") != -1 ){
        return true;
    }
    l = document.layers['popup'];
    l.src="$registryurl/who.cgi?email="+n+extra;

    if(d.target.y > window.innerHeight + window.pageYOffset - l.clip.height) {
         l.top = (window.innerHeight + window.pageYOffset - l.clip.height);
    } else {
         l.top = d.target.y - 6;
    }
    l.left = d.target.x - 6;

    l.visibility="show";
    return false;
}

function js_file_menu(dir,file,rev,root,d) {
    l = document.layers['popup'];
    l.src="$registryurl/file.cgi?file="+file+"&dir="+dir+"&rev="+rev+"&cvsroot="+root+"&linked_text="+d.target.text;
    
    if(d.target.y > window.innerHeight + window.pageYOffset - l.clip.height) { 
         l.top = (window.innerHeight + window.pageYOffset - l.clip.height);
    } else {
         l.top = d.target.y - 6;
    }

    l.left = d.target.x - 6;

    if( l.left + l.clipWidth > window.width ){
        l.left = window.width - l.clipWidth;
    }

    l.visibility="show";
    return false;
}

function hidePopup() {
}

//--></script>

<layer name="popup"  onMouseOut="this.visibility='hide';" left=0 top=0 bgcolor="#ffffff" visibility="hide">
</layer>

%;
} elsif ($::use_dom) {
$script_str = qq%
<script $::script_type><!--
var r;
var lock = 0;

function js_who_menu(n,extra,d) {
    if (r) {
        r.style.display='none';
    }
    var l = d.target.parentNode;

    r = document.getElementById('popupiframe');
    var url = "$registryurl/who.cgi?target=_parent&email="+n+extra;
    r.setAttribute("src",url);

    var t = l.offsetTop;
    var lf = l.offsetLeft;
    var p = l.offsetParent;
    while (p.tagName != 'BODY') {
        t = t + p.offsetTop;
        lf = lf + p.offsetLeft;
        p = p.offsetParent;
    }

    r.style.top = t;
    r.style.left = lf;
    r.style.display='block';
    return false;
}

function js_file_menu(dir,file,rev,root,d) {
    if (r) {
        r.style.display='none';
    }

    var l = d.target.parentNode;

    r = document.getElementById('popupiframe');
    var url = "$registryurl/file.cgi?file="+file+"&dir="+dir+"&rev="+rev+
        "&cvsroot="+root+"&target=_parent&linked_text="+l.firstChild.nodeValue;
    r.setAttribute("src",url);

    var t = l.offsetTop;
    var lf = l.offsetLeft;
    var p = l.offsetParent;
    while (p.tagName != 'BODY') {
        t = t + p.offsetTop;
        lf = lf + p.offsetLeft;
        p = p.offsetParent;
    }

    r.style.top = t;
    r.style.left = lf;
    r.style.display='block';
    lock = 1;
    return false;
}

function hidePopup() {
    if (!r) return;

    // this lock hack works around the fact that the click on the
    // link that opens the IFRAME also triggers a BODY onClick event
    // (see PutsHeader()) - only hide the popup if it is the second
    // click event we capture.
    if (lock > 1) {
        r.style.display='none';
        lock = 0;
    }
    lock = lock + 1;
}
//--></script>
<style type="text/css">
.popup {
    border-style: solid;
    border-color: #F0A000;
    background-color: #FFFFFF;
    padding: 0;
    position: absolute;
}
</style>
%;
} else {
$script_str = qq%
<script $::script_type><!--
function js_who_menu(n,extra,d) {}
function js_file_menu(dir,file,rev,root,d) {}
function hidePopup() {}
//--></script>
%;
}

}

